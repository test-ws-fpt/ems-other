package com.capstone.ems.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.capstone.ems.view.viewmodel.LoginScreenViewModel
import com.capstone.ems.R
import com.capstone.ems.databinding.ActivityLoginScreenBinding
import com.capstone.ems.view.viewmodel.LoginActivityViewModelFactory

/**
 ** This class used to declared viewModel and dataBinding of LoginScreen
 **/
class LoginActivity : AppCompatActivity() {

    private var binding: ActivityLoginScreenBinding? = null
    private lateinit var viewModel: LoginScreenViewModel
    private lateinit var viewModelFactory: LoginActivityViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewComponent()
    }


    private fun initViewComponent() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login_screen)
        viewModelFactory = LoginActivityViewModelFactory(this.application)
        viewModel = viewModelFactory.create(LoginScreenViewModel::class.java)
        binding?.loginViewModel = viewModel

        viewModel.loginLiveData?.observe(this, viewModel.resultObserver)
    }

    override fun onDestroy() {
        viewModel.loginLiveData?.removeObserver(viewModel.resultObserver)
        super.onDestroy()
    }
}