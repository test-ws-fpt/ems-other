package com.capstone.ems.view.viewmodel

import android.app.Application
import android.content.Intent
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import com.capstone.ems.hostp.HostConnectorImpl
import com.capstone.ems.hostp.LoginResult
import com.capstone.ems.util.Constant
import com.capstone.ems.view.activity.DraftActivity


/**
 ** This class used to viewmodel to handle configuraion changes
 **/
class LoginScreenViewModel(application: Application) : AndroidViewModel(application) {

    val username = MutableLiveData<String>()

    val password = MutableLiveData<String>()

    private val context = getApplication<Application>().applicationContext

    val resultObserver = Observer<LoginResult> { result ->
        when (result) {
            LoginResult.FAIL -> {
                Log.d(Constant.TAG, "login fail")
                Toast.makeText(context, "Login failed", Toast.LENGTH_SHORT).show()
                username.value = Constant.EMPTY_STRING
                password.value = Constant.EMPTY_STRING
            }
            LoginResult.SUCCESS -> {
                //start another activity
                Log.d("TAG", "login success")
                Toast.makeText(context, "Login success", Toast.LENGTH_SHORT).show()
                context.startActivity(
                    Intent(
                        context,
                        DraftActivity::class.java
                    )
                )
            }
        }
    }

    private val host = HostConnectorImpl()

    var loginLiveData: LiveData<LoginResult>? = null

    init {
        loginLiveData = MutableLiveData()
    }

    fun onBtnLoginClicked() {
        loginLiveData?.removeObserver(resultObserver)
        loginLiveData = host.login(
            username.value ?: Constant.EMPTY_STRING,
            password.value ?: Constant.EMPTY_STRING
        )
        loginLiveData?.observeForever(resultObserver)
    }

    fun onBtnRegisterClicked() {
        context.startActivity(
            Intent(
                context,
                DraftActivity::class.java
            )
        )
    }

    override fun onCleared() {
        loginLiveData?.removeObserver(resultObserver)
        super.onCleared()
    }

}