package com.capstone.ems.hostp.retrofit.model

import com.google.gson.annotations.SerializedName

class UserBodyResponse(

    @SerializedName("success")
    val status: String?,

    @SerializedName("access_token")
    val token: String?
)