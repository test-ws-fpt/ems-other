package com.capstone.ems.hostp.retrofit.model

import com.google.gson.annotations.SerializedName

/**
 * This class define data object for user sent to host.
 * @property username is the username.
 * @property password is the password.
 */
class UserBody (
    @SerializedName("username")
    val username: String?,

    @SerializedName("password")
    val password: String?
)