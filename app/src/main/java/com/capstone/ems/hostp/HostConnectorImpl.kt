package com.capstone.ems.hostp

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.capstone.ems.hostp.retrofit.ApiService
import com.capstone.ems.hostp.retrofit.model.UserBody
import com.capstone.ems.util.Constant
import kotlinx.coroutines.Dispatchers

/**
 * This class is the implementation of HostConnector interface.
 */
class HostConnectorImpl: HostConnector {

    private val apiService by lazy {
        ApiService.getInstance()
    }

    override fun login(
        username: String,
        password: String
    ): LiveData<LoginResult> {
        Log.d(Constant.TAG, "HostConnectorImpl #login $username $password")
        val data = liveData(Dispatchers.IO) {
            Log.d(Constant.TAG, "start live data scope in login function")
            val responseBody = apiService.login(UserBody(username, password))
            when (responseBody.status) {
                "true" -> {
                    Log.d(Constant.TAG, "success response")
                    emit(LoginResult.SUCCESS)
                }
                else -> {
                    Log.d(Constant.TAG, " response fail")
                    emit(LoginResult.FAIL)}
            }
        }

        return data
    }
}

enum class LoginResult {
    FAIL,
    SUCCESS
}