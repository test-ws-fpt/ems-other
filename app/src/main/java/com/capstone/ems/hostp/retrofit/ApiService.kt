package com.capstone.ems.hostp.retrofit

import com.capstone.ems.hostp.retrofit.model.UserBody
import com.capstone.ems.hostp.retrofit.model.UserBodyResponse
import com.capstone.ems.util.Constant
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * This interface define function of retrofit instance for connecting to host.
 */
interface ApiService {
    companion object {
        fun getInstance(): ApiService {
            val retrofit =
                Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constant.BASE_URL)
                    .build()
            return retrofit.create(ApiService::class.java)
        }
    }

    @POST("/auth/login")
    suspend fun login(
        @Body user: UserBody
    ): UserBodyResponse
}