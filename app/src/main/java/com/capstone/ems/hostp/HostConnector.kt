package com.capstone.ems.hostp

import androidx.lifecycle.LiveData

/**
 * This interface define apis for connecting to server.
 */
interface HostConnector {

    /**
     * This function define api for login
     * @param username is the username.
     * @param password is the password.
     */
    fun login(
        username: String,
        password: String
    ): LiveData<LoginResult>
}