package com.capstone.ems.util

/**
 * Define constant used in project.
 */
object Constant {

    const val TAG = "EMS"

    const val BASE_URL = "https://api.simplework.dev/"

    const val EMPTY_STRING = ""

}